<?php
/**
 * Created by ahmad.
 * Date: 11/28/18
 * Time: 9:04 PM
 */

namespace App\Providers;

use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Contracts\Auth\Authenticatable as UserContract;
use Illuminate\Contracts\Hashing\Hasher as HasherContract;

class CustomProvider extends EloquentUserProvider
{
    /**
     * CustomProvider constructor.
     * @param HasherContract $hasher
     * @param string $model
     */
    public function __construct(HasherContract $hasher, $model)
    {
        parent::__construct($hasher, $model);
    }

    /**
     * Validate a user against the given credentials.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable $user
     * @param  array $credentials
     * @return bool
     */
    public function validateCredentials(UserContract $user, array $credentials)
    {
        if (array_key_exists('password', $credentials)) {
            $plain = $credentials['password'];
            return $this->hasher->check($plain, $user->getAuthPassword());
        }
        return true;
    }


}