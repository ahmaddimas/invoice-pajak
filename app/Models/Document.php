<?php
/**
 * Created by ahmad.
 * Date: 11/22/18
 * Time: 10:11 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $table = "document";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'npwp', 'filename', 'downloaded_at', 'created_at', 'created_by', 'updated_by', 'updated_at',
    ];
}
