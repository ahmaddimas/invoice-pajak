<?php
/**
 * Created by ahmad.
 * Date: 11/21/18
 * Time: 9:55 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = "users";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'npwp', 'address', 'phone', 'password', 'email', 'email_verified_at', 'role', 'created_at', 'created_by', 'updated_by', 'updated_at',
    ];

    protected $hidden = [
        'password'
    ];
}
