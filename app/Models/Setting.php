<?php
/**
 * Created by ahmad.
 * Date: 11/21/18
 * Time: 9:25 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'setting';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'npwp', 'address', 'phone', 'email', 'number_digit', 'login', 'created_at', 'updated_at'
    ];
}
