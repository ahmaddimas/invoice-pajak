<?php

namespace App\Http\Middleware;

use App\Utilities\Constants;
use Closure;
use Illuminate\Support\Facades\Auth;

class RoleAuthentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $uri = $request->getRequestUri();
        if ($uri != Constants::USER_URL && !Auth::user()->role) {
            abort(403);
        }
        if ($uri === Constants::USER_URL && Auth::user()->role) {
            abort(403);
        }
        return $next($request);
    }
}
