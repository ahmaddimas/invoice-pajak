<?php

namespace App\Http\Middleware;

use App\Utilities\Constants;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            if (!Auth::user()->role)
                return redirect(Constants::USER_URL);
            return redirect(Constants::REDIRECT_URL);
        }

        return $next($request);
    }
}
