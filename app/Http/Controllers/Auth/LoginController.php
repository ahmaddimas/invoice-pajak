<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use App\Utilities\Constants;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected function redirectTo() {
        if (!Auth::user()->role)
            return Constants::USER_URL;
        return Constants::REDIRECT_URL;
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        return Constants::FIELD_NPWP;
    }

    protected function validateLogin(Request $request)
    {
        $credentials[$this->username()] = 'required|string';
        $condition[$this->username()] = $request->get($this->username());
        if (array_key_exists('password', $request->toArray())) {
            $credentials['password'] = 'required|string';
        }

        $user = User::where($condition)->first();
        if (!$request->get('user') && $user && $user->role) {
            throw ValidationException::withMessages([
                $this->username() => [trans('auth.failed')],
            ]);
        }
        if ($request->get('user') && $user && (!$user->role || $user->role == null)) {
            throw ValidationException::withMessages([
                $this->username() => [trans('auth.failed')],
            ]);
        }
        $request->validate($credentials);
    }

    protected function credentials(Request $request)
    {
        if (array_key_exists('password', $request->toArray()))
            return $request->only($this->username(), Constants::FIELD_PASSWORD);
        return $request->only($this->username());
    }


}
