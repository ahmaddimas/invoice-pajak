<?php

namespace App\Http\Controllers;

use App\Models\Document;
use App\Utilities\Constants;
use App\Utilities\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('pages.user');
    }

    public static function getAll()
    {
        try {
            $npwp = Auth::user()->npwp;
            return DataTables::eloquent(Document::where(Constants::FIELD_NPWP, $npwp))->make(true);
        } catch (\Exception $e) {
            return Response::failed($e->getMessage());
        }
    }
}
