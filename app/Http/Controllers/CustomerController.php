<?php
/**
 * Created by ahmad.
 * Date: 11/21/18
 * Time: 9:42 PM
 */

namespace App\Http\Controllers;


use App\Exceptions\Verification;
use App\Models\Customer;
use App\Models\Document;
use App\Utilities\Constants;
use App\Utilities\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Jenssegers\Agent\Agent;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use Yajra\DataTables\Facades\DataTables;

class CustomerController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth');
    }

    public static function getAll(Request $request)
    {
        try {
            $rows = Customer::where(Constants::FIELD_ROLE, "!=", 1)->orWhereNull(Constants::FIELD_ROLE)->get();

            foreach ($rows as $key => $row) {
                if ($row[Constants::FIELD_PHONE]) {
                    $agent = new Agent();
                    $url = 'https://web.whatsapp.com/send?phone='. $row[Constants::FIELD_PHONE];
                    if ($agent->isMobile())
                        $url = 'whatsapp://send?phone='. $row[Constants::FIELD_PHONE];
                    $row[Constants::WHATSAPP] = $url;
                }
            }

            return DataTables::collection($rows)->make(true);
        } catch (\Exception $e) {
            return Response::failed($e->getMessage());
        }
    }

    public static function getFiles()
    {
        try {
            Verification::check(Constants::FIELD_NPWP);
            $npwp = Input::get(Constants::FIELD_NPWP);
            return DataTables::eloquent(Document::where(Constants::FIELD_NPWP, $npwp))->make(true);
        } catch (Verification $e) {
            return Response::failed($e->getMessage());
        } catch (\Exception $e) {
            return Response::failed(Constants::STATUS_FAILURE);
        }
    }

    public static function add()
    {
        try {
            $customer = array();
            foreach (Input::get() as $key => $value) {
                if ($key == Constants::ID) continue;
                Verification::check($key);
                if ($key == Constants::FIELD_PASSWORD) {
                    $customer[$key] = Hash::make($value);
                    continue;
                }
                if ($key == Constants::FIELD_PHONE && $value[0] == 0) {
                    $customer[$key] = 62 . substr($value, 1);
                    continue;
                }
                $customer[$key] = $value;
            };
            $time = date('Y-m-d H:i:s');
            $customer[Constants::FIELD_CREATED_AT] = $time;
            $customer[Constants::FIELD_UPDATED_AT] = $time;

            Customer::create($customer);
            return Response::success("Successfully add customer");
        } catch (Verification $ve) {
            return Response::failed($ve->getMessage());
        } catch (\Exception $e) {

            return Response::failed($e->getMessage());
            return Response::failed(Constants::SERVER_FAILURE);
        }
    }

    public static function edit()
    {
        $post = Input::get();
        try {
            $customer = array();
            foreach ($post as $key => $value) {
                Verification::check($key);
                if ($key == Constants::ID || $key == Constants::FIELD_PASSWORD) continue;
                if ($key == Constants::FIELD_PHONE && $value[0] == 0) {
                    $customer[$key] = 62 . substr($value, 1);
                    continue;
                }
                $customer[$key] = $value;
            };
            $time = date('Y-m-d H:i:s');
            $customer[Constants::FIELD_UPDATED_AT] = $time;

            Customer::find($post[Constants::ID])->update($customer);

            return Response::success("Successfully update customer");
        } catch (Verification $ve) {
            return Response::failed($ve->getMessage());
        } catch (\Exception $e) {

            return Response::failed($e->getMessage());
            return Response::failed(Constants::SERVER_FAILURE);
        }
    }

    public static function delete()
    {
        try {
            Verification::check(Constants::CUSTOMER_ID);
            $customer_id = Input::get(Constants::CUSTOMER_ID);

            Customer::destroy($customer_id);

            return Response::success("Successfully delete customer");
        } catch (Verification $ve) {
            return Response::failed($ve->getMessage());
        } catch (\Exception $e) {

            return Response::failed($e->getMessage());
            return Response::failed(Constants::SERVER_FAILURE);
        }
    }

    public function import()
    {
        try {
            $fileName = time() . "_customer_import.xlsx";
            $tmpName  = $_FILES['file']['tmp_name'];
            $error    = $_FILES['file']['error'];
            $ext      = strtolower(pathinfo($fileName, PATHINFO_EXTENSION));

            if ( !in_array($ext, array('xlsx')) ) {
                throw new Verification("File must be xlsx ");
            }
            //if($fileType != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") throw new Verification("File must be xlsx ");

            if($error == UPLOAD_ERR_OK){
                $targetPath = public_path() . '/uploads/customer/' . $fileName;
                move_uploaded_file($tmpName,$targetPath);
                $this->doImport($targetPath);
                return  Response::success("Successfully import customer");
            }else{
                switch($error){
                    case UPLOAD_ERR_INI_SIZE:
                        throw new Verification('Error INI FILE SIZE');
                        break;
                    case UPLOAD_ERR_FORM_SIZE:
                        throw new Verification('Error UPLOAD_ERR_FORM_SIZE');
                        break;
                    case UPLOAD_ERR_PARTIAL:
                        throw new Verification('Error: UPLOAD_ERR_PARTIAL');
                        break;
                    case UPLOAD_ERR_NO_FILE:
                        throw new Verification('Error: UPLOAD_ERR_NO_FILE');
                        break;
                    case UPLOAD_ERR_NO_TMP_DIR:
                        throw new Verification('Error: UPLOAD_ERR_NO_TMP_DIR');
                        break;
                    case UPLOAD_ERR_CANT_WRITE:
                        throw new Verification('Error: UPLOAD_ERR_CANT_WRITE');
                        break;
                    case  UPLOAD_ERR_EXTENSION:
                        throw new Verification('Error: UPLOAD_ERR_EXTENSION');
                        break;
                    default: throw new Verification('Error: Unkown file error.');
                        break;
                }
            }
        }catch(Verification $ve){
            return Response::failed($ve->getMessage());
        }catch (\Exception $e) {
            return Response::failed($e->getMessage());
        }
    }

    private function doImport($path) {
        try {
            $reader = new Xlsx();
            $reader = $reader->load($path);
            $hasNext = true;
            $rowNum = 2;
            $customer = array();
            $i = 0;
            do {
                $activeCell = $reader->getActiveSheet()->getCellByColumnAndRow(1, $rowNum)->getValue();
                if ($activeCell == null && trim($activeCell) == '') {
                    $hasNext = false;
                    continue;
                }

                $temp[Constants::FIELD_NAME] = $reader->getActiveSheet()->getCellByColumnAndRow(1, $rowNum)->getValue();
                $temp[Constants::FIELD_NPWP] = $reader->getActiveSheet()->getCellByColumnAndRow(2, $rowNum)->getValue();
                $temp[Constants::FIELD_ADDRESS] = $reader->getActiveSheet()->getCellByColumnAndRow(3, $rowNum)->getValue();
                $temp[Constants::FIELD_PHONE] = $reader->getActiveSheet()->getCellByColumnAndRow(4, $rowNum)->getValue();
                $temp[Constants::FIELD_EMAIL] = $reader->getActiveSheet()->getCellByColumnAndRow(5, $rowNum)->getValue();

                if ($temp[Constants::FIELD_PHONE][0] == 0) {
                    $customer[Constants::FIELD_PHONE] = 62 . substr($customer[Constants::FIELD_PHONE], 1);
                }
                array_push($customer, $temp);
                $rowNum++;
                $i++;
            } while ($hasNext);

            if ($customer) {
                Customer::insert($customer);
            }
            return Response::success(Constants::STATUS_SUCCESS);
        } catch (\Exception $e) {
            return Response::failed($e->getMessage());
        }
    }
}
