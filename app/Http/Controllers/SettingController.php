<?php
/**
 * Created by ahmad.
 * Date: 11/21/18
 * Time: 10:26 PM
 */

namespace App\Http\Controllers;


use App\Exceptions\Verification;
use App\Models\Setting;
use App\Utilities\Constants;
use App\Utilities\Response;
use Illuminate\Support\Facades\Input;

class SettingController extends Controller
{
    public function __construct()
    {
//        $this->middleware('guest');
    }

    public function getAll() {
        /*return DataTables::eloquent(Setting::all())->make(true);*/
        try {
            return Response::success(Setting::all());
        } catch (\Exception $e) {
            return Response::failed($e->getMessage());
        }
    }

    public static function update()
    {
        try {
            $data = array();
            foreach (Input::get() as $key => $value) {
                // never use id, cause only one row
                // if ($key == Constants::ID) continue;
                Verification::check($key);
                $data[$key] = $value;
            };
            if (!array_key_exists(Constants::FIELD_LOGIN, $data))
                $data[Constants::FIELD_LOGIN] = 0;
            $time = date('Y-m-d H:i:s');
            $data[Constants::FIELD_UPDATED_AT] = $time;
            $data[Constants::FIELD_CREATED_AT] = $time;

            $setting = Setting::latest()->first();
            if (!$setting)
                $setting = new Setting();
            $setting->fill($data);
            $setting->save();
            return Response::success("Successfully update setting");
        } catch (Verification $ve) {
            return Response::failed($ve->getMessage());
        } catch (\Exception $e) {

            return Response::failed($e->getMessage());
            return Response::failed(Constants::SERVER_FAILURE);
        }
    }
}
