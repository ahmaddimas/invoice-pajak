<?php
/**
 * Created by ahmad.
 * Date: 11/22/18
 * Time: 10:10 PM
 */

namespace App\Http\Controllers;


use App\Exceptions\Verification;
use App\Models\Document;
use App\Utilities\Constants;
use App\Utilities\Response;
use Illuminate\Support\Facades\Input;
use Yajra\DataTables\Facades\DataTables;

class DocumentController extends Controller
{
    public function __construct()
    {
//        $this->middleware('guest');
    }

    public static function getAll()
    {
        return DataTables::eloquent(Document::query())->make(true);
    }

    public function add() {
        /*print_r(Input::get());
        echo "\n---------------\n";
        print_r($_FILES);
        exit;*/
        try {
            $name       = $_FILES['filename']['name'];
            $tmpName    = $_FILES['filename']['tmp_name'];
            $error      = $_FILES['filename']['error'];
            $size       = $_FILES['filename']['size'];
            $ext        = strtolower(pathinfo($name, PATHINFO_EXTENSION));
            $npwp       = explode('-', $name)[2];

            switch ($error) {
                case UPLOAD_ERR_OK:
                    $valid = true;
                    //validate file extensions
                    if ( !in_array($ext, array('pdf')) ) {
                        $valid = false;
                        $response = 'Invalid file extension.';
                    }
                    //validate file size
//                    if ( $size/1024/1024 > 2 ) {
//                        $valid = false;
//                        $response = 'File size is exceeding maximum allowed size.';
//                    }
                    //upload file
                    if ($valid) {
                        $time = time();
                        $fileName = $time ."_". $name;
                        $targetPath =  public_path() . DIRECTORY_SEPARATOR. 'uploads' .DIRECTORY_SEPARATOR.'document'.DIRECTORY_SEPARATOR. $fileName;
                        move_uploaded_file($tmpName,$targetPath);
                        $file = Document::create([
                            Constants::FIELD_NPWP           => $npwp,
                            Constants::FIELD_FILENAME       => $fileName,
                            Constants::FIELD_DOWNLOADED_AT  => null,
                            Constants::FIELD_CREATED_AT     => $time,
                            Constants::FIELD_UPDATED_AT     => $time
                        ]);
                        return Response::success("Successfully add document");
                        exit;
                    }
                    break;
                case UPLOAD_ERR_INI_SIZE:
                    $response = 'The uploaded file exceeds the upload_max_filesize directive in php.ini.';
                    break;
                case UPLOAD_ERR_FORM_SIZE:
                    $response = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.';
                    break;
                case UPLOAD_ERR_PARTIAL:
                    $response = 'The uploaded file was only partially uploaded.';
                    break;
                case UPLOAD_ERR_NO_FILE:
                    $response = 'No file was uploaded.';
                    break;
                case UPLOAD_ERR_NO_TMP_DIR:
                    $response = 'Missing a temporary folder. Introduced in PHP 4.3.10 and PHP 5.0.3.';
                    break;
                case UPLOAD_ERR_CANT_WRITE:
                    $response = 'Failed to write file to disk. Introduced in PHP 5.1.0.';
                    break;
                case UPLOAD_ERR_EXTENSION:
                    $response = 'File upload stopped by extension. Introduced in PHP 5.2.0.';
                    break;
                default:
                    $response = 'Unknown error';
                    break;
            }
            return Response::failed($response);
        } catch (\Exception $e) {
            return Response::failed($e->getMessage());
        }
    }

    public function delete() {
        try {
            Verification::check(Input::get(Constants::DOCUMENT_ID));
            $document_id = Input::get(Constants::DOCUMENT_ID);

            // remove file
            /*$document = Document::find($document_id);
            $targetPath =  public_path() . DIRECTORY_SEPARATOR. 'uploads' .DIRECTORY_SEPARATOR.'document'.DIRECTORY_SEPARATOR. $document->filename;
            unlink($targetPath);*/

            Document::destroy($document_id);
            return Response::success("Successfully delete document");
        } catch (\Exception $e) {
            return Response::failed($e->getMessage());
        }
    }

    public function download() {
        try {
            Verification::check(Constants::DOCUMENT_ID);
            $file = Document::where(Constants::FIELD_ID,Input::get(Constants::DOCUMENT_ID))->first();
            $file->downloaded_at = date('Y-m-d H:i:s');
            $file->save();
            return response()->download(public_path()."/uploads/document/".$file->filename, $file->filename);
        }catch (Verification $e) {
            return Response::failed($e->getMessage());
        } catch (\Exception $e) {
            return Response::failed($e->getMessage());
        }
    }
}
