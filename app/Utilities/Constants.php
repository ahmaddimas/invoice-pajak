<?php
/**
 * Created by ahmad.
 * Date: 10/25/18
 * Time: 11:16 AM
 */

namespace App\Utilities;


class Constants
{
    /* general */
    const REDIRECT_URL = "/customer";
    const USER_URL = "/home";

    /* field database */
    const FIELD_ID = "id";
    const FIELD_NPWP = "npwp";
    const FIELD_NAME = "name";
    const FIELD_EMAIL = "email";
    const FIELD_PASSWORD = "password";
    const FIELD_PHONE = "phone";
    const FIELD_ADDRESS = "address";
    const FIELD_FILENAME = "filename";
    const FIELD_STATUS = "status";
    const FIELD_NUMBER_DIGIT = "number_digit";
    const FIELD_LOGIN = "login";
    const FIELD_DOWNLOADED_AT = "downloaded_at";
    const FIELD_ROLE = "role";
    const FIELD_CREATED_AT = "created_at";
    const FIELD_UPDATED_AT = "updated_at";

    /* public identifier */
    const ID = "id";
    const CUSTOMER_ID = "id_customer";
    const DOCUMENT_ID = "id_document";
    const STATUS_ACTIVE = "active";
    const STATUS_INACTIVE = "inactive";
    const WHATSAPP = "whatsapp";

    const DATA = "data";
    const MESSAGE = "message";

    /* status success */
    const STATUS = "status";
    const STATUS_SUCCESS = "success";
    const STATUS_SUCCESS_CODE = 1;
    const STATUS_SUCCESS_OK = "ok";
    const STATUS_SUCCESS_OK_CODE = 200;
    const STATUS_SUCCESS_CREATED = "created";
    const STATUS_SUCCESS_CREATED_CODE = 201;
    const STATUS_SUCCESS_ACCEPTED = "accepted";
    const STATUS_SUCCESS_ACCEPTED_CODE = 202;
    const STATUS_SUCCESS_NO_CONTENT = "no content";
    const STATUS_SUCCESS_NO_CONTENT_CODE = 204;
    /* status failed */
    const STATUS_FAILED = "failed";
    const STATUS_FAILED_CODE = 0;
    const STATUS_FAILURE = "failure";
    const STATUS_FAILURE_CODE = 500;
    const STATUS_IS_EXIST = "Data already exist";
    const STATUS_NOT_FOUND = "Data not found";
}
