<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Authentication Routes...
Route::post('login', 'Auth\LoginController@login');
Route::any('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

Route::get('/', function () {
    return redirect()->route('login');
})->middleware('guest');

Route::get('/login', function () {
    $setting = \App\Models\Setting::all()->toArray()[0];
    $setting = $setting ? $setting : array('login' => 0);
    $setting['admin'] = 0;
    return view('auth.login')->with('setting', $setting);
})->middleware('guest')->name('login');

Route::get('/admin', function () {
    $setting = \App\Models\Setting::all()->toArray()[0];
    $setting = $setting ? $setting : array('login' => 0);
    $setting['admin'] = 1;
    return view('auth.login')->with('setting', $setting);
})->middleware('guest')->name('admin');


Route::middleware(['auth', 'role'])->group(function () {
    Route::get(\App\Utilities\Constants::USER_URL, 'UserController@index')->name('home');

    Route::get('/customer', function () {
        return view('pages.customer.index');
    });
    Route::get('/document', function () {
        return view('pages.document.index');
    });
    Route::get('/setting', function () {
        return view('pages.setting.index');
    });
});



Route::post('/api/v1/user/get_all', 'UserController@getAll');

Route::post('/api/v1/customer/get_all', 'CustomerController@getAll');
Route::post('/api/v1/customer/get_files', 'CustomerController@getFiles');
Route::post('/api/v1/customer/add', 'CustomerController@add');
Route::post('/api/v1/customer/import', 'CustomerController@import');
Route::post('/api/v1/customer/edit', 'CustomerController@edit');
Route::post('/api/v1/customer/delete', 'CustomerController@delete');

Route::any('/api/v1/document/get_all', 'DocumentController@getAll');
Route::post('/api/v1/document/add', 'DocumentController@add');
Route::post('/api/v1/document/delete', 'DocumentController@delete');
Route::get('/api/v1/document/download', 'DocumentController@download');

Route::post('/api/v1/setting/get_all', 'SettingController@getAll');
Route::post('/api/v1/setting/update', 'SettingController@update');
