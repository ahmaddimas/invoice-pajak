@extends('../layouts/default')

@section('title', 'Invoice')

@section('content')
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="feather icon-list bg-c-blue"></i>
                        <div class="d-inline">
                            <h5>Documents</h5>
                            <span>Daftar file document</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class=" breadcrumb breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="/"><i class="feather icon-home"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <div class="pcoded-inner-content">
            <!-- Main-body start -->
            <div class="main-body">
                <div class="page-wrapper">
                    <!-- Page-body start -->
                    <div class="page-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <!-- Ajax data source (objects) table start -->
                                <div class="card">
                                    <div class="card-header">
                                        <h5>List Document</h5>
                                    </div>
                                    <div class="card-block">
                                        <div class="table-responsive dt-responsive">
                                            <table id="main-table" class="table table-striped table-bordered nowrap" style="width:100%">
                                                <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>NPWP</th>
                                                    <th>File Name</th>
                                                    <th>Downloaded</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tfoot>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>NPWP</th>
                                                    <th>File Name</th>
                                                    <th>Downloaded</th>
                                                    <th>Action</th>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- Ajax data source (objects) table end -->
                            </div>
                        </div>
                    </div>
                    <!-- Page-body end -->
                </div>
            </div>
            <!-- Main-body end -->

            <div id="styleSelector">

            </div>
        </div>
    </div>
@stop

@section('js')
    <script>
        $(document).ready(() => {
            $('#main-table').DataTable({
                "ajax": {
                    "url": "/api/v1/user/get_all",
                    "type": "POST",
                    'beforeSend': function (request) {
                        request.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                    }
                },
                "processing": true,
                "serverSide": true,
                "columns": [{
                    "data": 'id'
                }, {
                    "data": 'npwp'
                }, {
                    "data": 'filename'
                }, {
                    "data": 'downloaded_at'
                }, {
                    "render": function (data, type, row) {
                        return "<center>" +
                            "<a href='/api/v1/document/download?id_document=" + row.id + "' target='_blank' class='btn btn-info'>" +
                            "<i class='fa text-white fa-download'></i>" +
                            "</a>" +
                            "</center>";
                    }
                }]
            });
        });
    </script>
@stop
