@extends('../layouts/default')

@section('title', 'Document')

@section('content')
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="feather icon-list bg-c-blue"></i>
                        <div class="d-inline">
                            <h5>Documents</h5>
                            <span>Daftar file document</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class=" breadcrumb breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="/"><i class="feather icon-home"></i></a>
                            </li>
                            <li class="breadcrumb-item"><a href="/document">Document</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <div class="pcoded-inner-content">
            <!-- Main-body start -->
            <div class="main-body">
                <div class="page-wrapper">
                    <!-- Page-body start -->
                    <div class="page-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <!-- Ajax data source (objects) table start -->
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Documents Table</h5>
                                    </div>
                                    <div class="card-block">
                                        <div class="table-responsive dt-responsive">
                                            <table id="main-table" class="table table-striped table-bordered nowrap" style="width:100%">
                                                <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>NPWP</th>
                                                    <th>File Name</th>
                                                    <th>Downloaded</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tfoot>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>NPWP</th>
                                                    <th>File Name</th>
                                                    <th>Downloaded</th>
                                                    <th>Action</th>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- Ajax data source (objects) table end -->
                            </div>
                        </div>
                    </div>
                    <!-- Page-body end -->
                </div>
            </div>
            <!-- Main-body end -->

            <div id="styleSelector">

            </div>
        </div>
    </div>
    @component('chunks.modal-document')
    @endcomponent
@stop

@section('js')
    <script>
        $(document).ready(function () {
            $('#main-table').DataTable({
                dom: 'Bfrtip',
                buttons: [{
                    text: 'Add Document',
                    className: 'btn waves-effect waves-light btn-primary btn-round',
                    action: function(e, dt, node, config) {
                        $('#form_modal').find('.modal-title').text('add document'.toUpperCase());
                        $('#form_modal').modal('show');
                    }
                }
                ],
                "ajax": {
                    "url": "/api/v1/document/get_all",
                    "type": "POST",
                    'beforeSend': function (request) {
                        request.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                    }
                },
                "processing": true,
                "serverSide": true,
                "columns": [{
                    "data": 'id'
                }, {
                    "data": 'npwp'
                }, {
                    "data": 'filename'
                }, {
                    "data": 'downloaded_at'
                }, {
                    "render": function (data, type, row) {
                        return "<center>" +
                            "<a href='/api/v1/document/download?id_document=" + row.id + "' target='_blank' class='btn waves-effect waves-light btn-info'>" +
                            "<i class='fa text-white fa-download'></i>" +
                            "</a>" +
                            "<button class='btn btn-danger' data-mode='delete' data-content='" + row.id + "'>" +
                            "<i class='fa fa-trash'></i>" +
                            "</button>" +
                            "</center>";
                    }
                }]
            });

            $(document).on('click', 'button[data-mode]', function (e) {
                var action = $(this).attr('data-mode');
                var data;
                if (action === 'delete') {
                    data = {'{{\App\Utilities\Constants::DOCUMENT_ID}}': $(this).attr('data-content')};
                } else {
                    data = new FormData($('#form')[0]);
                }

                if (action === 'delete') {
                    swal({
                        title: "Are you sure?",
                        text: "Once deleted, you will not be able to recover this file",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    }).then((confirm) => {
                        if (!confirm) {
                            return false;
                        } else {
                            deleteDocument(data);
                        }
                    });
                } else {
                    trigger(action, data);
                }

                return false;
            });

            $(document).on('hide.bs.modal', '#form_modal', function (e) {
                $('#form').trigger('reset');
            });
        });

        function trigger(action, data) {
            $.ajax({
                url: '/api/v1/document/' + action,
                type: 'POST',
                data: data,
                processData: false,
                contentType: false,
                beforeSend: function (req) {
                    req.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                },
                success: function (res) {
                    res = JSON.parse(res);
                    if (res.status) {
                        swal('Success', res.data, 'success', {buttons: false, timer: 1000});
                        $('#main-table').DataTable().ajax.reload();
                    } else {
                        swal('Failed', res.message, 'error', {buttons: false, timer: 2000});
                    }
                },
                failed: function (res) {
                    res = JSON.parse(res);
                    swal('Error', res.message, 'error', {buttons: false, timer: 2000});
                },
                complete: function () {
                    $('#form_modal').modal('hide');
                }
            });
            return false;
        }

        function deleteDocument(data) {
            $.ajax({
                url: '/api/v1/document/delete',
                type: 'POST',
                data: data,
                beforeSend: function (req) {
                    req.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                    swal('Loading', 'Please Wait', 'info', {buttons: false, timer: 1500});
                },
                success: function (res) {
                    res = JSON.parse(res);
                    if (res.status) {
                        swal('Success', res.data, 'success', {buttons: false, timer: 1000});
                        $('#main-table').DataTable().ajax.reload();
                    } else {
                        swal('Failed', res.message, 'error', {buttons: false, timer: 2000});
                    }
                },
                failed: function (res) {
                    res = JSON.parse(res);
                    swal('Error', res.message, 'error', {buttons: false, timer: 2000});
                }
            });
            return false;
        }
    </script>
@stop
