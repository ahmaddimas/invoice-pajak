@extends('../layouts/default')

@section('title', 'Setting')

@section('content')
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="feather icon-list bg-c-blue"></i>
                        <div class="d-inline">
                            <h5>Setting</h5>
                            <span>Data pengaturan</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class=" breadcrumb breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="/"><i class="feather icon-home"></i></a>
                            </li>
                            <li class="breadcrumb-item"><a href="/setting">Setting</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <div class="pcoded-inner-content">
            <!-- Main-body start -->
            <div class="main-body">
                <div class="page-wrapper">
                    <!-- Page-body start -->
                    <div class="page-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <!-- Ajax data source (objects) table start -->
                                <div class="card">
                                    <div class="card-block">
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs md-tabs" role="tablist">
                                            <li class="nav-item">
                                                <a href="#company" class="nav-link active show" data-toggle="tab"role="tab">Company Detail</a>
                                                <div class="slide"></div>
                                            </li>
                                            <li class="nav-item">
                                                <a href="#general" class="nav-link" data-toggle="tab" role="tab">General</a>
                                                <div class="slide"></div>
                                            </li>
                                        </ul>
                                        <!-- Tab panes -->
                                        <div class="tab-content card-block">
                                            <div class="tab-pane active" id="company" role="tabpanel">
                                                <form action="" role="form" id="company_form">
                                                    <div class="form-group form-inline">
                                                        <label for="name" class="control-label col-md-3 col-offset-md-1">Company Name</label>
                                                        <input type="text" class="form-control underlined col-md-7" id="name" name="name" placeholder="Company Name"
                                                               autofocus>
                                                    </div>
                                                    <div class="form-group form-inline">
                                                        <label for="npwp" class="control-label col-md-3 col-offset-md-1">NPWP</label>
                                                        <input type="text" class="form-control underlined col-md-7" id="npwp" name="npwp" placeholder="NPWP"
                                                               autofocus>
                                                    </div>
                                                    <div class="form-group form-inline">
                                                        <label for="address" class="control-label col-md-3 col-offset-md-1">Address</label>
                                                        <textarea name="address" id="address" rows="3" class="form-control underlined col-md-7"
                                                                  placeholder="Address"></textarea>
                                                    </div>
                                                    <div class="form-group form-inline">
                                                        <label for="phone" class="control-label col-md-3 col-offset-md-1">Phone</label>
                                                        <input type="number" class="form-control underlined col-md-7" id="phone" name="phone"
                                                               placeholder="Phone Number" autofocus>
                                                    </div>
                                                    <div class="form-group form-inline">
                                                        <label for="email" class="control-label col-md-3 col-offset-md-1">Email</label>
                                                        <input type="email" class="form-control underlined col-md-7" id="email" name="email"
                                                               placeholder="Email Address" autofocus>
                                                    </div>
                                                    {{--<div class="col-md-12 text-center">
                                                        <button type="reset" class="btn btn-warning-outline col-md-3">Reset</button>
                                                        <button type="submit" class="btn btn-success-outline col-md-3" data-mode="add">Submit</button>
                                                    </div>--}}
                                                </form>
                                            </div>
                                            <div class="tab-pane" id="general" role="tabpanel">
                                                <form action="" role="form" id="general_form">
                                                    <div class="form-group form-inline">
                                                        <label for="number_digit" class="control-label col-md-3 col-offset-md-1">Digit Nomor Faktur</label>
                                                        <input type="text" class="form-control underlined col-md-7" id="number_digit" name="number_digit" placeholder="Digit Nomor Faktur"
                                                               autofocus>
                                                    </div>
                                                    <div class="form-group form-inline">
                                                        <label for="login" class="control-label col-md-3 col-offset-md-1">Login System</label>
                                                        <div class="border-checkbox-section">
                                                            <div class="border-checkbox-group border-checkbox-group-primary">
                                                                <input type="checkbox" class="border-checkbox" id="login" name="login" value="1">
                                                                <label class="border-checkbox-label" for="login">Jika dicentang, maka menggunakan system login dengan npwp dan password</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {{--<div class="col-md-12 text-center">
                                                        <button type="reset" class="btn btn-warning-outline col-md-3">Reset</button>
                                                        <button type="submit" class="btn btn-success-outline col-md-3" data-mode="add">Submit</button>
                                                    </div>--}}
                                                </form>
                                            </div>
                                        </div>
                                        <div class="col-md-12 text-center mt-3">
                                            <button type="reset" class="btn btn-warning-outline col-md-3">Reset</button>
                                            <button type="submit" class="btn btn-success-outline col-md-3" data-mode="add">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Ajax data source (objects) table end -->
                        </div>
                    </div>
                </div>
                <!-- Page-body end -->
            </div>
        </div>
        <!-- Main-body end -->

        <div id="styleSelector">

        </div>
    </div>
    </div>
@stop

@section('js')
    <script>
        $(document).ready(function () {
            getSetting();

            $(document).on('click', 'button[data-mode="add"]', function (e) {
                var data = $('#company_form').serialize() +"&"+ $('#general_form').serialize();

                trigger(data);

                return false;
            });
        });

        function getSetting() {
            $.ajax({
                url: '/api/v1/setting/get_all',
                type: 'POST',
                beforeSend: function (req) {
                    req.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                },
                success: function (res) {
                    res = JSON.parse(res);
                    var data = res.data[0];
                    if (res.status) {
                        $('#company_form').find('#name').val(data.name);
                        $('#company_form').find('#npwp').val(data.npwp);
                        $('#company_form').find('#address').val(data.address);
                        $('#company_form').find('#phone').val(data.phone);
                        $('#company_form').find('#email').val(data.email);
                        $('#general_form').find('#number_digit').val(data.number_digit);
                        $('#general_form').find('#login').prop('checked', data.login == 1);
                    } else {
                        swal('Failed', res.message, 'error', {buttons: false, timer: 2000});
                    }
                },
                failed: function (res) {
                    res = JSON.parse(res);
                    swal('Error', res.message, 'error', {buttons: false, timer: 2000});
                }
            });
            return false;
        }

        function trigger(data) {
            $.ajax({
                url: '/api/v1/setting/update',
                type: 'POST',
                data: data,
                beforeSend: function (req) {
                    req.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                },
                success: function (res) {
                    res = JSON.parse(res);
                    if (res.status) {
                        swal('Success', res.data, 'success', {buttons: false, timer: 1000});
                    } else {
                        swal('Failed', res.message, 'error', {buttons: false, timer: 2000});
                    }
                },
                failed: function (res) {
                    res = JSON.parse(res);
                    swal('Error', res.message, 'error', {buttons: false, timer: 2000});
                }
            });
            return false;
        }
    </script>
@stop
