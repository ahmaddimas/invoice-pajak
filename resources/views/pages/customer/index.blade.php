@extends('../layouts/default')

@section('title', 'Customers')

@section('content')
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="feather icon-list bg-c-blue"></i>
                        <div class="d-inline">
                            <h5>Customers</h5>
                            <span>Daftar data customers</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class=" breadcrumb breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="/"><i class="feather icon-home"></i></a>
                            </li>
                            <li class="breadcrumb-item"><a href="/customer">Customer</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <div class="pcoded-inner-content">
            <!-- Main-body start -->
            <div class="main-body">
                <div class="page-wrapper">
                    <!-- Page-body start -->
                    <div class="page-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <!-- Ajax data source (objects) table start -->
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Customers Table</h5>
                                    </div>
                                    <div class="card-block">
                                        <div class="table-responsive dt-responsive">
                                            <table id="main-table" class="table table-striped table-bordered nowrap" style="width:100%">
                                                <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Name</th>
                                                    <th>NPWP</th>
                                                    <th>Address</th>
                                                    <th>Phone</th>
                                                    <th>Email</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tfoot>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Name</th>
                                                    <th>NPWP</th>
                                                    <th>Address</th>
                                                    <th>Phone</th>
                                                    <th>Email</th>
                                                    <th>Action</th>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- Ajax data source (objects) table end -->
                            </div>
                        </div>
                    </div>
                    <!-- Page-body end -->
                </div>
            </div>
            <!-- Main-body end -->

            <div id="styleSelector">

            </div>
        </div>
    </div>
    @component('../../chunks/modal-customer')
    @endcomponent
    @component('chunks.modal-import-customer')
    @endcomponent
    @component('chunks.modal-detail-customer')
    @endcomponent
@stop

@section('js')
    <script>
        $(document).ready(function () {
            $('#main-table').DataTable({
                dom: 'Bfrtip',
                buttons: [{
                        text: 'Import from excel',
                        className: 'btn waves-effect waves-light btn-primary btn-round',
                        action: function(e, dt, node, config) {
                            $('#import_modal').modal('show');
                        }
                    }, {
                        text: 'Add Customer',
                        className: 'btn waves-effect waves-light btn-primary btn-round',
                        action: function(e, dt, node, config) {
                            $('#form').find('button[data-mode="edit"]').attr('data-mode', 'add');
                            $('#form_modal').find('.modal-title').text('add customer'.toUpperCase());
                            $('#form_modal').modal('show');
                            $('#password').parent().show();
                        }
                    }
                ],
                "ajax": {
                    "url": "/api/v1/customer/get_all",
                    "type": "POST",
                    'beforeSend': function (request) {
                        request.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                    }
                },
                "processing": true,
                "serverSide": true,
                "columns": [{
                    "data": 'id'
                }, {
                    "data": 'name'
                }, {
                    "data": 'npwp'
                }, {
                    "data": 'address'
                }, {
                    "data": 'phone'
                }, {
                    "data": 'email'
                }, {
                    "render": function (data, type, row) {
                        let whatsapp = "<a href='"+ row.whatsapp +"' class='btn waves-effect waves-light btn-success btn-sm btn-round'>" +
                            "<i class='icofont icofont-social-whatsapp' style='margin: 0; font-size: 14px;'></i>" +
                            "</a>";
                        let call = "<a href='tel:"+ row.phone +"' class='btn waves-effect waves-light btn-facebook btn-sm btn-round'>" +
                            "<i class='icofont icofont-phone' style='margin: 0; font-size: 14px;'></i>" +
                            "</a>";
                        let email = "<a href='mailto:"+ row.email +"' class='btn waves-effect waves-light btn-google-plus btn-sm btn-round'>" +
                            "<i class='feather icon-mail' style='margin: 0; font-size: 14px;'></i>" +
                            "</a>";
                        whatsapp = row.whatsapp ? whatsapp : '';
                        call = row.phone ? call : '';
                        email = row.email ? email : '';
                        const btnSocial = whatsapp + call + email;
                        return "<center>" +
                                btnSocial +
                                "<button class='btn waves-effect waves-light btn-info btn-sm btn-round' role='view' data-content='" + row.npwp + "' onclick='setFormData(" + JSON.stringify(row) + ")'>" +
                                "<i class='feather icon-eye' style='margin: 0; font-size: 14px;'></i>" +
                                "</button>" +
                                "<button class='btn waves-effect waves-light btn-primary btn-sm btn-round' role='edit' onclick='setFormData(" + JSON.stringify(row) + ")'>" +
                                "<i class='feather icon-edit-2' style='margin: 0; font-size: 14px;'></i>" +
                                "</button>" +
                                "<button class='btn waves-effect waves-light btn-danger btn-sm btn-round' data-mode='delete' data-content='" + row.id + "'>" +
                                "<i class='feather icon-trash' style='margin: 0; font-size: 14px;'></i>" +
                                "</button>" +
                                "</center>";
                    }
                }]
            });

            $(document).on('click', 'button[role]', function (e) {
                var action = $(this).attr('role');
                if (action === 'edit') {
                    $('#form').find('button[data-mode="add"]').attr('data-mode', 'edit');
                    $('#form_modal').find('.modal-title').text('edit customer'.toUpperCase());
                    $('#form_modal').modal('show');
                    $('#password').parent().hide();
                } else if (action === 'view') {
                    loadFiles($(this).attr('data-content'));
                    $('#detail_modal').modal('show');
                }
            });

            $(document).on('click', 'button[data-mode]', function (e) {
                var action = $(this).attr('data-mode');
                var data;
                if (action === 'delete') {
                    data = {'{{\App\Utilities\Constants::CUSTOMER_ID}}': $(this).attr('data-content')};
                } else if (action === 'import') {
                    data = new FormData($('#import_form')[0]);
                } else {
                    data = new FormData($('#form')[0]);
                }

                if (action === 'delete') {
                    swal({
                        title: "Are you sure?",
                        text: "Once deleted, you will not be able to recover this file",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    }).then((confirm) => {
                        if (!confirm) {
                            return false;
                        } else {
                            deleteCustomer(data);
                        }
                    });
                } else {
                    triggerCustomer(action, data);
                }

                return false;
            });

            $(document).on('hide.bs.modal', '#form_modal, #import_modal', function (e) {
                $('#form').trigger('reset');
            });
        });

        function loadFiles(id) {
            $('#files-table').DataTable().destroy();
            $('#files-table').DataTable({
                "ajax": {
                    "url": "/api/v1/customer/get_files",
                    "type": "POST",
                    "data": {'{{\App\Utilities\Constants::FIELD_NPWP}}': id},
                    'beforeSend': function (request) {
                        request.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                    }
                },
                "processing": true,
                "serverSide": true,
                "columns": [{
                    "data": 'id'
                }, {
                    "data": 'npwp'
                }, {
                    "data": 'filename'
                }, {
                    "data": 'downloaded_at'
                }, {
                    "render": function (data, type, row) {
                        return "<center>" +
                            "<a href='/api/v1/document/download?id_document=" + row.id + "' target='_blank' class='btn btn-info'>" +
                            "<i class='fa text-white fa-download'></i>" +
                            "</a>" +
                            "</center>";
                    }
                }]
            });
        }

        function setFormData(data) {
            $('#id').val(data.id);
            $('#name, #detail_name').val(data.name);
            $('#npwp, #detail_npwp').val(data.npwp);
            $('#address, #detail_address').val(data.address);
            $('#phone, #detail_phone').val(data.phone);
            $('#email, #detail_email').val(data.email);
            //$('#password').val(data.password);
        }

        function triggerCustomer(action, data) {
            $.ajax({
                url: '/api/v1/customer/' + action,
                type: 'POST',
                data: data,
                processData: false,
                contentType: false,
                beforeSend: function (req) {
                    req.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                    swal('Loading', 'Please Wait', 'info', {buttons: false, timer: 1500});
                },
                success: function (res) {
                    res = JSON.parse(res);
                    if (res.status) {
                        swal('Success', res.data, 'success', {buttons: false, timer: 1000});
                        $('#main-table').DataTable().ajax.reload();
                    } else {
                        swal('Failed', res.message, 'error', {buttons: false, timer: 2000});
                    }
                },
                failed: function (res) {
                    res = JSON.parse(res);
                    swal('Error', res.message, 'error', {buttons: false, timer: 2000});
                },
                complete: function () {
                    $('#import_modal').modal('hide');
                    $('#form_modal').modal('hide');
                }
            });
            return false;
        }

        function deleteCustomer(data) {
            $.ajax({
                url: '/api/v1/customer/delete',
                type: 'POST',
                data: data,
                beforeSend: function (req) {
                    req.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                    swal('Loading', 'Please Wait', 'info', {buttons: false, timer: 1500});
                },
                success: function (res) {
                    res = JSON.parse(res);
                    if (res.status) {
                        swal('Success', res.data, 'success', {buttons: false, timer: 1000});
                        $('#main-table').DataTable().ajax.reload();
                    } else {
                        swal('Failed', res.message, 'error', {buttons: false, timer: 2000});
                    }
                },
                failed: function (res) {
                    res = JSON.parse(res);
                    swal('Error', res.message, 'error', {buttons: false, timer: 2000});
                }
            });
            return false;
        }
    </script>
@stop
