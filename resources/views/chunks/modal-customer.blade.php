@extends('../chunks/modal')
@section("modalTitle")
    title
@stop
@section("modalContent")
    <form action="" role="form" id="form">
        <input type="hidden" id="id" name="id" value="">
        <div class="form-group form-inline">
            <label for="name" class="control-label col-md-3 col-offset-md-1">Name</label>
            <input type="text" class="form-control underlined col-md-7" id="name" name="name" placeholder="Name"
                   autofocus>
        </div>
        <div class="form-group form-inline">
            <label for="npwp" class="control-label col-md-3 col-offset-md-1">NPWP</label>
            <input type="text" class="form-control underlined col-md-7" id="npwp" name="npwp" placeholder="NPWP"
                   autofocus>
        </div>
        <div class="form-group form-inline">
            <label for="address" class="control-label col-md-3 col-offset-md-1">Address</label>
            <textarea name="address" id="address" rows="3" class="form-control underlined col-md-7"
                      placeholder="Address"></textarea>
        </div>
        <div class="form-group form-inline">
            <label for="phone" class="control-label col-md-3 col-offset-md-1">Phone</label>
            <input type="number" class="form-control underlined col-md-7" id="phone" name="phone"
                   placeholder="Phone Number" autofocus>
        </div>
        <div class="form-group form-inline">
            <label for="email" class="control-label col-md-3 col-offset-md-1">Email</label>
            <input type="email" class="form-control underlined col-md-7" id="email" name="email"
                   placeholder="Email Address" autofocus>
        </div>
        <div class="form-group form-inline">
            <label for="password" class="control-label col-md-3 col-offset-md-1">Password</label>
            <input type="password" class="form-control underlined col-md-7" id="password" name="password"
                   placeholder="Password" autofocus>
        </div>
        <div class="col-md-12 text-center">
            <button type="button" class="btn waves-effect waves-light col-md-3" data-dismiss="modal" aria-label="Close">
                Close
            </button>
            <button type="reset" class="btn waves-effect waves-light btn-warning col-md-3">Reset</button>
            <button type="submit" class="btn waves-effect waves-light btn-success col-md-3" data-mode="add">Submit</button>
        </div>
    </form>
@stop
