<nav class="pcoded-navbar">

    <div class="nav-list">
        <div class="pcoded-inner-navbar main-menu">
            <div class="pcoded-navigation-label">Navigation</div>
            <ul class="pcoded-item pcoded-left-item">
                @if(\Illuminate\Support\Facades\Auth::user()->role)
                    <li class="">
                        <a href="/customer" class="waves-effect waves-dark">
                          <span class="pcoded-micon">
                            <i class="feather icon-users"></i>
                          </span>
                            <span class="pcoded-mtext">Customer</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="/document" class="waves-effect waves-dark">
                          <span class="pcoded-micon">
                            <i class="feather icon-file"></i>
                          </span>
                            <span class="pcoded-mtext">Document</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="/setting" class="waves-effect waves-dark">
                          <span class="pcoded-micon">
                            <i class="feather icon-settings"></i>
                          </span>
                            <span class="pcoded-mtext">Setting</span>
                        </a>
                    </li>
                @else
                    <li class="active">
                        <a href="/home" class="waves-effect waves-dark">
                          <span class="pcoded-micon">
                            <i class="feather icon-file"></i>
                          </span>
                            <span class="pcoded-mtext">Document</span>
                        </a>
                    </li>
                @endif
            </ul>

        </div>
    </div>
</nav>