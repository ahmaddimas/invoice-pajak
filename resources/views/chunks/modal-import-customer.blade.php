<div class="modal fade" id="import_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Import Customer</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
            </div>
            <div class="modal-body">
                <form role="form" method="post" id="import_form" enctype="multipart/form-data">
                    <div class="form-group form-inline">
                        <label class="control-label col-md-3 col-offset-md-1">Template Excel</label>
                        <a href="/samples/template_data_customer.xlsx" target="_blank" class="btn waves-effect waves-light btn-info">
                            <i class="feather icon-download"></i>
                        </a>
                    </div>
                    <div id="formFileUpload" class="form-group form-inline">
                        <label for="file" class="control-label col-md-3 col-offset-md-1">File Excel</label>
                        <input type="file" id="file" name="file" class="form-control col-md-7">
                    </div>
                    <center>
                        <button type="button" class="btn btn-danger-outline col-md-2" data-dismiss="modal"
                                aria-label="Close">Close
                        </button>
                        <button type="submit" class="btn btn-success-outline col-md-2" data-mode="import">Submit</button>
                    </center>
                </form>
            </div>
        </div>
    </div>
</div>
