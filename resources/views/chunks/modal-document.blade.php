@extends('../chunks/modal')
@section("modalTitle")
    Add Document
@stop
@section("modalContent")
    <form action="" role="form" id="form" method="post" enctype="multipart/form-data">
        <div class="form-group form-inline">
            <label for="filename" class="control-label col-md-3 col-offset-md-1">File</label>
            <input type="file" class="form-control col-md-7" id="filename" name="filename">
        </div>
        <center>
            <button type="button" class="btn btn-danger-outline col-md-2" data-dismiss="modal" aria-label="Close">Close</button>
            <button type="button" class="btn btn-success-outline col-md-2" data-mode="add">Submit</button>
        </center>
    </form>
@stop
