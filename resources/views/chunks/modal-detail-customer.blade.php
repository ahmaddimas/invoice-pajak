<div class="modal fade" id="detail_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" style="display: block;">
                <h5 class="modal-title">Details Customer</h5>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs md-tabs" role="tablist">
                    <li class="nav-item">
                        <a href="#detail" class="nav-link active show" data-toggle="tab"role="tab">Detail Customer</a>
                        <div class="slide"></div>
                    </li>
                    <li class="nav-item">
                        <a href="#files" class="nav-link" data-toggle="tab" role="tab">Customer Files</a>
                        <div class="slide"></div>
                    </li>
                </ul>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <!-- Tab panes -->
                        <div class="tab-content card-block">
                            <div class="tab-pane active" id="detail" role="tabpanel">
                                <div class="form-group form-inline">
                                    <label for="name" class="control-label col-md-3 col-offset-md-1">Name</label>
                                    <input type="text" class="form-control underlined col-md-7" id="detail_name" readonly>
                                </div>
                                <div class="form-group form-inline">
                                    <label for="npwp" class="control-label col-md-3 col-offset-md-1">NPWP</label>
                                    <input type="text" class="form-control underlined col-md-7" id="detail_npwp" readonly>
                                </div>
                                <div class="form-group form-inline">
                                    <label for="address" class="control-label col-md-3 col-offset-md-1">Address</label>
                                    <textarea id="detail_address" rows="3" class="form-control underlined col-md-7" readonly></textarea>
                                </div>
                                <div class="form-group form-inline">
                                    <label for="phone" class="control-label col-md-3 col-offset-md-1">Phone</label>
                                    <input type="number" class="form-control underlined col-md-7" id="detail_phone" readonly>
                                </div>
                                <div class="form-group form-inline">
                                    <label for="email" class="control-label col-md-3 col-offset-md-1">Email</label>
                                    <input type="email" class="form-control underlined col-md-7" id="detail_email" readonly>
                                </div>
                            </div>
                            <div class="tab-pane" id="files" role="tabpanel">
                                <div class="table-responsive dt-responsive">
                                    <table id="files-table" class="table table-striped table-bordered nowrap" style="width:100%">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>NPWP</th>
                                            <th>File Name</th>
                                            <th>Downloaded</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>ID</th>
                                            <th>NPWP</th>
                                            <th>File Name</th>
                                            <th>Downloaded</th>
                                            <th>Action</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Ajax data source (objects) table end -->
                </div>
            </div>
        </div>
    </div>
</div>